import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Overview } from '../pages/overview/overview';
import { Settings } from '../pages/settings/settings';
import { DHBWService } from '../service/background';
import { MySearchbar } from '../pages/settings/searchbar';

import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { ResponsiveModule, ResponsiveConfig } from 'ng2-responsive';
import { BackgroundMode } from '@ionic-native/background-mode';
import { CacheModule } from "ionic-cache";


let config = {
  breakPoints: {
    xs: { max: 600 },
    s: { min: 601, max: 640 },
    sm: { min: 641, max: 768 },
    m: { min: 769, max: 896 },
    md: { min: 897, max: 1024 },
    d: { min: 1024, max: 1280 },
    lg: { min: 1281, max: 1664 },
    l: { min: 1665, max: 1920 },
    xl: { min: 1921 }
  },
  debounceTime: 100 // allow to debounce checking timer
};

export function ResponsiveDefinition() {
  return new ResponsiveConfig(config);
};

@NgModule({
  declarations: [
    MyApp,
    Overview,
    Settings,
    MySearchbar
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ResponsiveModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CacheModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Overview,
    Settings
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    //Storage,
    BackgroundMode,
    DHBWService
  ]
})

export class AppModule { }
