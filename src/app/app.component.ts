import { Component, ViewChild, Input } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { Overview } from '../pages/overview/overview';
import { Settings } from '../pages/settings/settings';
import { DHBWService } from '../service/background';
import { CacheService } from "ionic-cache";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @Input() personname: string;

  // make HelloIonicPage the root (or first) page
  rootPage: any = Overview;
  private pages: Array<{ title: string, subtitle: string, image: string, component: any }>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public service: DHBWService,
    public cache: CacheService,
  ) {
    this.personname = "Login";
    this.cache.setDefaultTTL(60 * 60); //set default cache TTL for 1 hour

    if (this.platform.is('cordova')) {
      this.service.start();
      this.service.listenActivate();
    }
    this.initializeApp();
    // set our app's pages
    this.pages = [
      { title: 'Vorlesungsplan', subtitle: "", image: "../assets/img/vorlesungsplan.jpg", component: 'LecturesPage' },
      { title: 'Mensa', subtitle: "", image: "../assets/img/mensa.jpg", component: 'CanteenPage' },
      { title: 'Öffendliche Verkehrsmittel', subtitle: "", image: "../assets/img/oeffi.jpg", component: 'PublicTransportPage' },
      { title: 'Einstellungen', subtitle: "", image: "", component: Settings }
    ];

  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
  openLogin() {
    this.menu.close();
    this.nav.push("LoginPage", { callback: this.myCallbackFunction });
  }
  public myCallbackFunction = (name) => {
    return new Promise((resolve, reject) => {
      this.personname = name;
      resolve();
    });
  }
}
