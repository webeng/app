import { DayEvents } from './day-events';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        DayEvents
    ],
    exports: [
      DayEvents
    ]
})
export class PipesModule {}
