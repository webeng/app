import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment/moment';

@Pipe({ name: 'weekday',  pure: true})
export class DayEvents implements PipeTransform {
    static date: moment.Moment = moment();
    static options = {weekday: "long", year: "numeric", month: "short",
    day: "numeric"};

    transform(value:number, args:number[]): string {
        return DayEvents.date
                        .year(args[0])
                        .week(args[1]+1) //week count starts with 1, not with 0
                        .day(value+1) //day count starts with 1, not with 0
                        .toDate().toLocaleDateString("de-DE", DayEvents.options);
    }
}
