import {Injectable} from '@angular/core';

import { BackgroundMode } from '@ionic-native/background-mode';

@Injectable()
export class DHBWService {
  constructor(private backgroundMode: BackgroundMode) {
  }
  start(){
    //alert("Warum?");
    console.log("Go Barnie");
    this.backgroundMode.enable();
  }
  listenActivate(){
    this.backgroundMode.on('activate').subscribe(observer => {
      console.log("Background is on!");
      setInterval(function () {
          this.plugins.notification.badge.increase();
      }, 1000);
    });
  }
}
