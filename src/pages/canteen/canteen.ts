import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage, Slides } from 'ionic-angular';
import { Http } from '@angular/http';

import { Overview }  from '../../pages/overview/overview';
import Cheerio from 'cheerio';
import * as moment from 'moment/moment'; // http://www.joe0.com/2016/09/22/how-to-use-moment-js-in-angular-2/

/*
  Generated class for the Canteen page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-canteen',
  templateUrl: 'canteen.html'
})
export class CanteenPage {
  @ViewChild(Slides) slides: Slides;
  public menueTable: Array<Array<JSON>>;
  public weekdays = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController) {
    let loading = loadingCtrl.create({ content: 'Loading Data' });
    loading.present();
    var htmlapi = "https://www.stw-ma.de/speiseplan_mensaria_metropol-date-" + moment().format('YYYY_MM_DD') + "-pdfView-1.html";
    this.http.get(htmlapi)
      .toPromise().then((response) => {
        var $ = Cheerio.load(response.text());
        this.menueTable = this.parseText($);
        loading.dismiss();
      }).catch((reason: any) => {
        console.log(reason);
        loading.dismiss();
      })//.finally(loading.dismiss());
  }

  ngAfterViewInit() {
    this.menueTable = new Array<Array<JSON>>(5);
    var today = new Date().getDay();
    this.slides.initialSlide = (today < 5) ? today : 0;
  }
  parseText($: any): Array<Array<JSON>> {
    var menueTable = [];
    $('table#previewTable > tr').each(function(i, elementRow) {
      // Row 0 = Headerzeile(Menue1, Menue2...), ungerade = Mahlzeiten pro Tag, Gerade(!=0) = Preise pro Gericht
      var day = Math.floor((i + 1) / 2);
      menueTable[day] = [[], [], [], [], [], []];
      if (i != 0) {
        $(this).children('td').each(function(k, elementCol) {
          if (k != 0) {
            if (i % 2 == 1) {
              var menu = $(this).children('p')
              //console.log("Menü " + k + ": " + menu.html());
              menueTable[day - 1][k - 1]["menu"] = menu.text().trim();
            } else {
              //console.log("Preis " + (k) + ": " + $(this).html());
              menueTable[day - 1][k - 1]["price"] = $(this).text().trim();
            }
          }
        });
      }
    });
    return menueTable.slice(0, 5);
  }

  nextDay() {
    this.slides.slideNext();
  }

  previousDay() {
    this.slides.slidePrev();
  }
  back() {
    this.navCtrl.setRoot(Overview);
  }

}
