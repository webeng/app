import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanteenPage } from './canteen';

@NgModule({
  declarations: [
    CanteenPage,
  ],
  imports: [
    IonicPageModule.forChild(CanteenPage),
  ],
  exports: [
    CanteenPage
  ]
})
export class CanteenPageModule {}
