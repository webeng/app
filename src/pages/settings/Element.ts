export class Element{
  public readonly id:string;
  public readonly label: string;
  public readonly normalizedLabel;

  constructor(id: string, label:string){
    this.id = id;
    this.label = label;
    this.normalizedLabel = Element.normalizeLabel(label);
  }

  public static  normalizeLabel(label:string):string{
    return label.toLowerCase().replace(/[\- ]/g, "");
  }

}
