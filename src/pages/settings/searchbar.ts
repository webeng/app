import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Element } from './Element';

@Component({
  selector: 'mysearchbar',
  templateUrl: 'searchbar.html'
})
export class MySearchbar {
  @Input() elements: Element[] = [];
  private filteredElements: Element[] = this.elements;
  @Input() placeholder: string = "Suche";
  private _selectedLabel: string = null;
  @Input() set selectedLabel(selectedLabel: string) {
    this._selectedLabel = selectedLabel;
    this.searchInput = selectedLabel
  };
  get selectedLabel(): string { return this._selectedLabel; }
  private searchInput: string = this.selectedLabel;
  @Output() select: EventEmitter<Element> = new EventEmitter();

  //http://stackoverflow.com/questions/40846825/detect-usage-of-variables-in-angular-2-template-literal
  setElement(element: Element) {
    console.log("Element: " + element.label);
    this.selectedLabel = element.label;
    this.select.emit(element);
    this.filteredElements = new Array<Element>();
  }

  //http://stackoverflow.com/questions/40846825/detect-usage-of-variables-in-angular-2-template-literal
  filterElements(event) {
    let input = event.target.value;
    if (input != null && input.length > 0) {
      let normalizedInput = Element.normalizeLabel(input);
      this.filteredElements = this.elements.filter((kurs: Element) => kurs.normalizedLabel.startsWith(normalizedInput));
    }
    else
      this.filteredElements = new Array<Element>();
  }


}
