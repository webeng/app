import { Component } from '@angular/core';
import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { NavController, NavParams } from 'ionic-angular';
import { Overview }  from '../../pages/overview/overview';

import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import { Element } from './Element';
//import { MySearchbar } from './searchbar';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class Settings {
  private kursID: string;
  private kursLabel: string;
  private kurseLink: string = "http://vorlesungsplan.dhbw-mannheim.de/ical.php";
  private kurse: Array<Element> = new Array<Element>();
  private stationLabel: string;
  private stations: Array<Element> = [];
  private stationsLink: string = "http://rnv.the-agent-factory.de:8080/easygo2/api/regions/rnv/modules/stations/packages/1"
  private RNV_API_TOKEN = "cpbtj0fcbhvi0fbggas1e4nlrq";
  private locations = [
    { title: 'Coblitzalle', name: 'coblitzalle', selected: false },
    { title: 'Käfertal', name: 'kafertal', selected: false },
    { title: 'Eppelheim', name: 'eppelheim', selected: false }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public http: Http) {
    this.loadLocation();
    this.loadKurse();
    this.loadStations();
  }

  private loadStations() {
    this.storage.ready().then(() => this.storage.get('homeStation').then((data) => this.stationLabel = data));
    let header = new Headers({ "RNV_API_TOKEN": this.RNV_API_TOKEN });
    let options = new RequestOptions({
      method: RequestMethod.Get,
      headers: header,
      url: this.stationsLink
    });
    this.http.get(this.stationsLink, options).subscribe((response) => {
      let stations: { hafasID: string, longName: string }[] = response.json().stations;
      this.stations = stations.map((h) => new Element(h.hafasID, h.longName));
    });

  }

  private loadLocation() {
    this.storage.ready().then(() => {
      this.storage.get('standort').then((data) => {
        console.log("load standort: " + data);
        for (let location of this.locations) {
          location.selected = location.name === data;
        }
      })
    });
  }

  private loadKurse() {
    this.http.get(this.kurseLink).subscribe((response) => {
      let responseDom: Document = new DOMParser().parseFromString(response.text(), "text/html");
      let kurseSelect: HTMLElement = responseDom.getElementsByName("Kurse")[0];
      let options: NodeList = kurseSelect.getElementsByTagName("option");
      let kurse: Element[] = new Array();
      for (let option in options) {
        let attributes = options[option].attributes;
        if (attributes == null)
          continue;
        let id = attributes.getNamedItem("value").nodeValue;
        if (id == "Kurs auswählen" || id == null)
          continue;
        let label = attributes.getNamedItem("label").nodeValue;
        if (label == null)
          continue;
        kurse.push(new Element(id, label));
      }
      this.kurse = kurse;

      this.storage.ready().then(() => {
        this.storage.get('kursID').then((data) => {
          if (data != null) {
            for (let kurs of kurse) {
              if (kurs.id == data)
                this.kursLabel = kurs.label;
            }
          }
        })
      });
    });
  }

  ionViewDidLoad() {
    this.storage.ready().then(() => {
      this.storage.get('kursID').then((data) => {
        this.kursID = data;
        console.log("kursID: " + data);
      });
    });
    console.log(this.locations);
  }



  setKurs(kurs: Element) {
    console.log("set Kurs: " + kurs.label)
    this.kursID = kurs.id;
    this.saveKurs();
  }

  saveKurs(): void {
    this.storage.ready().then(() => this.storage.set('kursID', this.kursID));
    console.log("save kurs: " + this.kursID);
  }

  saveLocation(standort: string) {
    this.storage.ready().then(() => this.storage.set('standort', standort));
    console.log("save standort: " + standort);
  }
  //http://stackoverflow.com/questions/40846825/detect-usage-of-variables-in-angular-2-template-literal
  setStation(station: Element) {
    this.storage.ready().then(() => {
      this.storage.set('homeStationID', station.id);
      this.storage.set('homeStation', station.label);
    });
  }
  async isLocation(standort: string) {
    Promise.resolve(
      this.storage.ready().then(() => this.storage.get(standort).then((data) =>
        data.name == standort
      ))
    );
  }

  back() {
    this.navCtrl.setRoot(Overview);
  }
}
