import { Component, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-overview',
  templateUrl: 'overview.html'
})
export class Overview {
  public img: HTMLImageElement;
  pages: Array<{title: string, subtitle: string, image: string, component: any}>;
  constructor(public navCtrl: NavController, public el: ElementRef) {
    this.pages = [
      { title: 'Vorlesungsplan', subtitle: "", image: "vorlesungsplan.jpg", component: 'LecturesPage' },
      { title: 'Mensa', subtitle: "", image: "mensa.jpg", component: 'CanteenPage' },
      { title: 'Öffendliche Verkehrsmittel', subtitle: "", image: "oeffi.jpg", component: 'PublicTransportPage' }
    ];
  }
  ionViewDidLoad(){  }
  openPage(page) {
    // navigate to the new page if it is not the current page
    this.navCtrl.setRoot(page.component);
  }
}
