import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LecturesPage } from './lectures';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    LecturesPage,
  ],
  imports: [
    IonicPageModule.forChild(LecturesPage),
    PipesModule
  ],
  exports: [
    LecturesPage
  ]
})
export class LecturesPageModule {}
