import { Component, ViewChild } from '@angular/core';
import { NavController, Slides, LoadingController, IonicPage, AlertController } from 'ionic-angular';
import { Overview }  from '../../pages/overview/overview';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/toPromise';
import * as moment from 'moment/moment';
import Ical2Json from 'ical2json';
import { Course } from './course';
import { CacheService } from "ionic-cache";
import { Settings } from '../settings/settings';

@IonicPage()
@Component({
  selector: 'page-lectures',
  templateUrl: 'lectures.html',
  providers: [Course],
})
export class LecturesPage {
  @ViewChild(Slides) slides: Slides;
  kursID: string;
  slideNumbers: Array<number>;
  today: moment.Moment;
  currentWeek: number;
  currentYear: number;
  weekEvents: Array<Array<Course>>;
  events: Observable<any>;
  weekdays: Array<string>;
  calendar: JSON;

  constructor(public navCtrl: NavController,
    public storage: Storage,
    public http: Http,
    public loadingCtrl:LoadingController,
    public cache: CacheService,
    public alertCtrl: AlertController) {
      let loading = loadingCtrl.create({content: 'Loading course schedule...', dismissOnPageChange: true});
      loading.present();
      new Promise((resolve, reject) => { //always reload -> ineffective
          this.storage.ready().then(() => this.storage.get('kursID').then((kursID) =>{
            this.kursID=kursID;
            console.log("Get Courseplan for this ID: " + kursID);
            this.calendar = JSON.parse('{}');;
          })).then(()=>{
            console.log("Get Course Schedule");
            console.log(this.calendar)
            var courseSchedule = this.getCourseSchedule();
            if(courseSchedule != null){
              courseSchedule.subscribe(res =>{
                res.forEach((event)=>{
                let start = moment(event['DTSTART'], "YYYYMMDDTHHmmss");
                let end = moment(event['DTEND'], "YYYYMMDDTHHmmss");
                var weekNum = start.week() -1;
                var yearNum = start.year();
                var dayNum = start.weekday()-1;
                if(!this.calendar.hasOwnProperty(yearNum)){
                  this.calendar[yearNum]= new Array<Array<Course>>(53);
                  for(let week = 0; week< this.calendar[yearNum].length; ++week){
                    this.calendar[yearNum][week]=new Array<Course>(6);
                    for(let day = 0; day< this.calendar[yearNum][week].length; ++day){
                      this.calendar[yearNum][week][day]=new Array<Course>(0);
                    }
                  }
                }
                var course = new Course(event['SUMMARY'],
                start.toDate(),
                end.toDate(),
                event['LOCATION']);
                this.calendar[yearNum][weekNum][dayNum].push(course); // Pushed again and again
              });
              console.log("Result");
              console.log(res);
              this.initializeWeekEvents();
          })}});
          console.log("End Constructor");
          resolve();
      }).then(()=> loading.dismiss());
    }

    getCourseSchedule(){
      if(this.kursID==null || this.kursID===""){
        let alert = this.alertCtrl.create({
            title: 'No course schedule available',
            subTitle: 'Have you entered your course ID?',
            buttons: [{
                      text: 'Go to settings',
                      handler: () => {
                        this.navCtrl.setRoot(Settings);
                      }},
                      {
                      text: 'Back',
                      handler: () => {
                        this.navCtrl.setRoot(Overview);
                      }}
                ]
          });
          alert.present();
          console.log("is null");
          return null;
        } else {
          var link = "http://vorlesungsplan.dhbw-mannheim.de/ical.php?uid="+ this.kursID;
          let request = this.http.get(link).map((response) => {
            return Ical2Json.convert(response.text())["VCALENDAR"][0]["VEVENT"];
          },(error)=>{
            console.log("Couldn't load course schedule: ");
            console.log(error);
            console.log(error.name);
          });
          console.log('Before cache ' + link);
          var response;
          try{
            response = this.cache.loadFromObservable(this.kursID, request);
          } catch(error){
            response = request;
          }
          return response;
        }
      }

    initializeWeekEvents(){
      if(this.calendar[this.currentYear]!==undefined){
        this.weekEvents = this.calendar[this.currentYear][this.currentWeek];
        console.log(this.weekEvents);
      }
    }

    updateView() {
      let backward = this.slides.getActiveIndex()<this.slides.getPreviousIndex();
      if (backward) {
        this.goToPrevWeek();
      } else {
        this.goToNextWeek();
      }
    }

    goToNextWeek(){
      let nextSlide = this.slides.getActiveIndex();
      if (this.slides.isEnd()) {
        this.slideNumbers.push(this.slideNumbers[this.slideNumbers.length - 1] + 1);
        this.slideNumbers.shift();
        nextSlide--;
      }
      if(this.currentWeek===52){
        this.currentYear++;
        this.currentWeek = 0;
      }else
        this.currentWeek++;
      this.initializeWeekEvents();
      this.slides.slideTo(1, 0, false);
    }

    goToPrevWeek(){
      let nextSlide = this.slides.getActiveIndex();
      if (this.slides.isBeginning()) {
        this.slideNumbers.unshift(this.slideNumbers[0] - 1);
        this.slideNumbers.pop();
        nextSlide++;
      }
      if(this.currentWeek===0){
        this.currentYear--;
        this.currentWeek=52;
      }else
        this.currentWeek--;
      this.initializeWeekEvents();
      this.slides.slideTo(1, 0, false);
    }

    showCurrentWeek(){
      this.currentWeek = this.today.week()-1;
      this.currentYear = this.today.year();
      this.initializeWeekEvents();
      this.slides.slideTo(1, 0, false);
    }

    ngAfterViewInit(){
      this.weekdays = new Array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
      this.slideNumbers = new Array<number>(-1,0,1);  //slider initializations: creates infinit slider

      this.weekEvents = new Array<Array<Course>>(6);
      for(let i = 0; i< this.weekEvents.length; ++i){
        this.weekEvents[i]=new Array<Course>();
      }

      this.calendar = JSON;
      this.today = moment();
      this.currentWeek = this.today.week();
      this.currentYear = this.today.year();
    }

    ngOnInit() {
      this.slides.initialSlide = 1;//current week
    }

    back(){
      this.navCtrl.setRoot(Overview);
    }
  }
