import { Inject } from '@angular/core'

export class Course {
  summary: string;
  start: Date;
  end: Date;
  location: string;

  constructor(@Inject('[object String]') public sum: string,
              @Inject('[object Date]') startDate: Date,
              @Inject('[object Date]') endDate: Date,
              @Inject('[object String]') public loc: string){
    this.summary = sum;
    this.start = startDate;
    this.end = endDate;
    this.location = loc;
  }
}
