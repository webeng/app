import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController } from 'ionic-angular';
import { Headers, RequestOptions, RequestMethod, Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import { Overview }  from '../../pages/overview/overview';
import { Settings } from '../settings/settings';
/*
Generated class for the PublicTransport page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-public-transport',
  templateUrl: 'public-transport.html'
})
export class PublicTransportPage {
  token: string = "cpbtj0fcbhvi0fbggas1e4nlrq";
  haltestellenDHAbfahrten10Min: string = "http://rnv.the-agent-factory.de:8080/easygo2/api/regions/rnv/modules/stationmonitor/element?time=null&hafasID=";
  rnv: JSON;
  private dhStations = { eppelheim: 697, kafertal: 2404, coblitzalle: 2521 }
  private segments = [{ label: 'DH', name: 'dh' }, { label: 'Zuhause', name: 'home' }];
  private _selectedSegment;
  set selectedSegment(segment) {
    this._selectedSegment = segment;
    if (segment == 'dh')
      this.loadDhDepartues();
    if (segment == 'home')
      this.loadHomeDepartues();
  }
  get selectedSegment() { return this._selectedSegment; }

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage: Storage, public alertCtrl: AlertController) {
    this.selectedSegment = this.segments[0].name;
  }

  private loadDhDepartues() {
    this.storage.ready().then(() => this.storage.get('standort').then((location) => {
      if (location == null || location === "") {
        let alert = this.alertCtrl.create({
          title: 'Keine Hochschule ausgewählt',
          subTitle: 'Haben Sie eine Hochschule ausgewählt?',
          buttons: [{
            text: 'Go to settings',
            handler: () => {
              this.navCtrl.setRoot(Settings);
            }
          },
            {
              text: 'Back',
              handler: () => {
                this.navCtrl.setRoot(Overview);
              }
            }
          ]
        });
        alert.present();
      }
      else
        this.loadDepartures(this.dhStations[location]);
    }));
  }

  private loadHomeDepartues() {
    this.storage.ready().then(() => this.storage.get('homeStationID').then((id) => {
      if (id == null || id === "") {
        let alert = this.alertCtrl.create({
          title: 'Keine Haltestelle ausgewählt',
          subTitle: 'Haben Sie eine Heimathaltestelle ausgewählt?',
          buttons: [{
            text: 'Go to settings',
            handler: () => {
              this.navCtrl.setRoot(Settings);
            }
          },
            {
              text: 'Back',
              handler: () => {
                this.navCtrl.setRoot(Overview);
              }
            }
          ]
        });
        alert.present();
      }
      else
        this.loadDepartures(id);
    }));
  }

  private loadDepartures(stationID: string) {
    let url = this.haltestellenDHAbfahrten10Min + stationID;
    let header = new Headers({ "RNV_API_TOKEN": this.token });
    let options = new RequestOptions({
      method: RequestMethod.Get,
      headers: header,
      url: url
    });
    this.http.get(url, options).subscribe((response) => {
      this.rnv = response.json().listOfDepartures;
      console.log(this.rnv);
    }
    );
  }

  ionViewDidLoad() { }

  back() {
    this.navCtrl.setRoot(Overview);
  }
}
