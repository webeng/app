import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicTransportPage } from './public-transport';

@NgModule({
  declarations: [
    PublicTransportPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicTransportPage),
  ],
  exports: [
    PublicTransportPage
  ]
})
export class PublicTransportPageModule {}
