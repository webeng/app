import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moodle_client from 'moodle-client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  private callback;
  private personname;
  private loginForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder
  ) {
    this.loginForm = formBuilder.group({
      moodleName: ['value', Validators.compose([Validators.pattern('(s|S)[0-9]{6}'), Validators.required])],
      moodleSecret: ['value', Validators.compose([Validators.minLength(1), Validators.required])]
    });
  }

  ionViewWillEnter() {
    this.callback = this.navParams.get("callback");
  }
  ionViewWillLeave() {
    this.callback(this.personname).then(() => { });
  }
  test_moodle(username: string, passord: string) {
    moodle_client.init({
      wwwroot: "http://moodle.dhbw-mannheim.de",
      username: username,
      password: passord
    }).then((client) => {
      console.log(client);
      this.access_moodle(client);
    }).catch((err) => {
      console.log("Unable to initialize the client: " + err);
      this.personname = "Hallo";
    });
  }
  access_moodle(client): void {
    console.log("Hi");
    client.call({
      wsfunction: "core_webservice_get_site_info",
    }).then((info) => {
      console.log("Hello %s, welcome to %s", info.fullname, info.sitename);
      this.personname = 'Hallo ' + info.fullname;
      this.navCtrl.pop();
    });
  }

}
