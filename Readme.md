Inofficial APP DHBW Mannheim
==================================================
Vorraussetzungen
---------------------------------------------------
- **Node.js**  
Website: https://nodejs.org/en/
- **Ionic & Apache Cordova**  
Website: https://ionicframework.com/getting-started/
```
npm install -g cordova ionic
```
- **(optional) Atom**  
Website: https://atom.io/  
Mit folgenden Plugins:
```
apm install autocomplete-plus linter
apm install atom-typescript angular-2-typeScript-snippets
apm install autocomplete-ionic2-framework ionic-preview
```

Ausführung
-----------------------------------------
### Im Browser ausführen:
```
cd app
npm install
npm install --only=dev
ionic serve
```
### Fürs Handy Compilieren  
Android mit USB Debugging aktiviert.
- Java JDK installieren & JAVA_HOME setzen
- Android SDK installieren &  ANDROID_HOME setzen
```
ionic platform add android
ionic run android --device
```
